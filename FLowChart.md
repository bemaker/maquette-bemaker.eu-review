```mermaid
graph TD
subgraph home
a1(menu) --- a2(slider 4/5 images )
a2 --- a3(about)
subgraph Call to action
a3 --- a3.5(TEXTE introductif au projet bemaker)
a3.5 --- a4(CTA) 
a4 --- inscription 
a4 --- decouvrir 
a4 --- atelier 
decouvrir --- a5(cta btn MORE)
inscription --- a5(cta btn MORE)
atelier --- a5(cta btn MORE)
end
subgraph LEARN
a5 --- a6(LEARN PANEL)
a6 --- a6.5(texte desctiptif du rapport théorie / pratique)
a6.5 --- a6.6(Tutoriels)
a6.5 --- a6.7(Ateliers)
end
subgraph workshop
a6.6 --- WORKSHOP
a6.7 --- WORKSHOP
end
WORKSHOP --- FOOTER
end


```

```mermaid
graph LR

a(menu)---l(left)
a --- r(right)
l --- about
about --- a1(descriptif projet bemaker)
a1 --- a2(description des enjeux)
a2 --- a3(ecosystème)
l --- l0(Learn)
l0 --- l1(tutoriels)
l0 --- l2(ateliers)
l --- w0(workshop)
w0 --- w1(liste d'événements dans les lieux partenaires et autres)
l --- c0(contact)
c0 --- c1(équipe + Mailto: ?)
c0 --- c2(formulaire)
r --- login(LOGIN / welcome xyz)
login --- user(access to user account page, badges etc)
r --- subscribe
r --- t0(translate)
t0 --- FR
t0 --- NL

```



